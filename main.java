public class main{
    public static void main(String[] array){
        String[] arrayNames = new String[]{"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] arraySurnames = new String[]{"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};


        for (int i = 1; i < arrayNames.length; i++) {
            for (int j = 0; j < arrayNames.length - i; j++) {
                if (arrayNames[j].compareTo(arrayNames[j + 1]) > 0) {
                    String tempName = arrayNames[j];
                    arrayNames[j] = arrayNames[j + 1];
                    arrayNames[j + 1] = tempName;

                    String tempSurname = arraySurnames[j];
                    arraySurnames[j] = arraySurnames[j + 1];
                    arraySurnames[j + 1] = tempSurname;
                }

                if (arrayNames[j].compareTo(arrayNames[j + 1]) == 0) {
                    if (arraySurnames[j].compareTo(arraySurnames[j + 1]) > 0) {
                        String tempName = arrayNames[j];
                        arrayNames[j] = arrayNames[j + 1];
                        arrayNames[j + 1] = tempName;

                        String tempSurname = arraySurnames[j];
                        arraySurnames[j] = arraySurnames[j + 1];
                        arraySurnames[j + 1] = tempSurname;
                    }
                }
            }
        }


        for (String arrayName : arrayNames) {
            System.out.print(arrayName + " ");
        }
        System.out.println();
        for (String arraySurname : arraySurnames) {
            System.out.print(arraySurname + " ");
        }
    }


}
